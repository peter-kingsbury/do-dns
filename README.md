# do-dns

## usage
```
$ node do-dns.js --help
Usage: do-dns.js -c [/path/to/config.json]

Options:
  --version     Show version number                                    [boolean]
  -c, --config  Use the provided configuration file.    [default: "config.json"]
  -h, --help    Show help                                              [boolean]

```

## sample configuration:
```json
{
  "digitalOcean": {
    "apiToken": "your-digital-ocean-api-key"
  },
  "targetDomain": {
    "hostname": "your-domain-name"
  },
  "logger": {
    "logFilePath": "/path/to/your/logfile.log"
  },
  "forceUpdate": true
}
```
