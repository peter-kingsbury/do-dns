'use strict';

const argv = require('yargs')
    .usage('Usage: $0 -c [/path/to/config.json]')
    .alias('c', 'config')
    .describe('c', 'Use the provided configuration file.')
    .nargs('c', 1)
    .default('c', 'config.json')
    .help('h')
    .alias('h', 'help')
    .argv;

const rp = require('request-promise-native');
const { promisify } = require('util');
const getIP = promisify(require('external-ip')());
const path = require('path');

function logToFile(file, text) {
  const fs = require('fs');
  const stream = fs.createWriteStream(file, { flags:'a' });
  stream.write(`${new Date().toISOString()} ${text}` + '\n');
  stream.end();
}

(async () => {

  /****************************************************************************
   * Load config.
   */

  const configPath = path.join(process.cwd(), argv.config);
  let config = null;
  try {
    config = require(configPath);
  } catch (e) {
    console.log(e.message);
    throw new Error(`failed to load config ${configPath}`);
  }

  /****************************************************************************
   * Destructure config into variables.
   */

  const { digitalOcean: {
    apiToken
  }, targetDomain: {
    hostname
  }, logger: {
    logFilePath
  }, forceUpdate } = config;

  /****************************************************************************
   * Get external IP address.
   */

  let ip = null;

  try {
    ip = await getIP();
  } catch (e) {
    logToFile(logFilePath, e.message);
    throw new Error(`failed to obtain external IP`);
  }

  /****************************************************************************
   * Load DNS records from DigitalOcean.
   */

  let records = null;

  try {
    records = await rp({
      method: 'get',
      uri: `https://api.digitalocean.com/v2/domains/${hostname}/records`,
      auth: {
        'bearer': apiToken
      },
      json: true
    });
  } catch (e) {
    logToFile(logFilePath, e.message);
    throw new Error(`failed to fetch DNS records`);
  }

  /****************************************************************************
   * Find the A-record for the domain.
   */

  const { domain_records } = records;
  let aRecord = null;

  try {
    aRecord = domain_records.find(rec => {
      return rec.type.toLowerCase() === 'a';
    });
  } catch(e) {
    logToFile(logFilePath, e.message);
    throw new Error(`unable to locate A record for ${hostname}`);
  }

  /****************************************************************************
   * If the A-record IP address matches the current external IP address, exit.
   */

  let log = null;
  const { data, id } = aRecord;
  if (data === ip && forceUpdate === false) {
    log = `External IP address ${ip} has not changed; exiting.`;
    logToFile(logFilePath, log);
    console.log(log);
    process.exit(0);
  }
  /****************************************************************************
   * Otherwise, update the A-record to the external IP address.
   */

  try {
    await rp({
      method: 'put',
      uri: `https://api.digitalocean.com/v2/domains/${hostname}/records/${id}`,
      auth: {
        'bearer': apiToken
      },
      json: true,
      body: {
        data: ip
      },
      resolveWithFullResponse: true
    });

    log = `External IP address ${ip} successfully updated for domain '${hostname}'.`;
    logToFile(logFilePath, log);
    console.log(log);
  } catch (e) {
    logToFile(logFilePath, e.message);
    throw new Error(`failed to update DNS entry for host '${hostname}': ${e.message}`);
  }
})();
